package main

import (
	"context"
	"testing"

	"github.com/stretchr/testify/suite"
	portal "gitlab.com/mergetb/api/portal/v1/go"
)

type UserTestSuite struct {
	suite.Suite // struct composition to include all suite deatils.

	opsCtx   context.Context   // login context of ops
	userCtx  context.Context   // login context of user
	userData *UserRegisterData // user data
}

// Invoke thte test suite via a normal go test
func TestUserCrud(t *testing.T) {
	suite.Run(t, &UserTestSuite{})
}

func (uts *UserTestSuite) SetupSuite() {

	uts.userData = murphUser

	var err error
	uts.opsCtx, err = LoginContext(opsId, opsPw, uts.Assertions)
	uts.Nil(err, ErrorStr(err))

	err = AddActiveUser(uts.userData, uts.opsCtx, uts.Assertions)
	uts.Nil(err, ErrorStr(err))

	uts.T().Logf("created user: %+v", uts.userData)

}

func (uts *UserTestSuite) TearDownSuite() {
	err := DeleteActiveUser(
		uts.userData.Username,
		uts.opsCtx,
		uts.Assertions,
	)
	uts.Nil(err, ErrorStr(err))
}

func (uts *UserTestSuite) TestUserCreate() {

	// the user has been created and is active at this point so just check the data.
	user, err := GetUser(uts.opsCtx, uts.userData.Username)
	uts.Require().Nil(err, ErrorStr(err))

	uts.T().Log("user", user)

	// Confirm the user data is correct
	uts.Equal(portal.UserState_Active, user.State)
	uts.Equal(uts.userData.Username, user.Username)
	uts.Equal(uts.userData.Name, user.Name)
	uts.NotZero(user.Gid)
	uts.NotZero(user.Uid)
	uts.False(user.Admin)
	uts.Empty(user.Facilities)
	uts.Empty(user.Organizations)
	uts.NotEmpty(user.Projects)
	uts.Contains(user.Projects, uts.userData.Username) // personal project exists.
	uts.Len(user.Projects, 1)                          // only one project - the personal project
	uts.Equal(portal.Member_Creator, user.Projects[uts.userData.Username].Role)
	uts.Equal(portal.Member_Active, user.Projects[uts.userData.Username].State)
	uts.Equal(uts.userData.Institution, user.Institution)
	uts.Equal(uts.userData.Category, user.Category)
	uts.Equal(uts.userData.Email, user.Email)
	uts.Equal(uts.userData.Country, user.Country)
	uts.Equal(uts.userData.Usstate, user.Usstate)
}

func (uts *UserTestSuite) TestUserLogin() {

	// Confirm the user can login
	ctx, err := LoginContext(
		uts.userData.Username,
		uts.userData.Password,
		uts.Assertions,
	)
	uts.Nil(err, ErrorStr(err))
	uts.T().Logf("got login context for %s: +%v", uts.userData.Username, ctx)
}
