package main

import (
	"context"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/mergetb/portal/services/pkg/merror"
	"google.golang.org/grpc/codes"
)

//
// Tests of Identity API
//

type IdentityTestSuite struct {
	suite.Suite // struct composition to include all suite deatils.

	opsCtx context.Context // login context of ops
}

// Invoke thte test suite via a normal go test
func TestIdentityAPI(t *testing.T) {
	suite.Run(t, &IdentityTestSuite{})
}

func (its *IdentityTestSuite) SetupSuite() {

	// get ops login context so ew can unregister as the ops user.
	var err error
	its.opsCtx, err = LoginContext(opsId, opsPw, its.Assertions)
	its.Nil(err, ErrorStr(err))
}

func (its *IdentityTestSuite) BeforeTest(suiteName, testName string) {
	its.T().Logf("Before Test %s", testName)
	err := Register(murphUser)
	its.Nil(err, ErrorStr(err))
}

func (its *IdentityTestSuite) AfterTest(suiteName, testName string) {
	its.T().Logf("After Test %s", testName)
	err := DeleteUser(its.opsCtx, murphUser.Username)
	its.Nil(err, ErrorStr(err))
}

// TestRegisterUser - tests register
func (its *IdentityTestSuite) TestRegisterUser() {

	// confirm the new ID is there.
	id, err := GetIdentity(its.opsCtx, murphUser.Username)
	its.Require().Nil(err, ErrorStr(err))
	its.Equal(murphUser.Username, id.Username)
	its.T().Logf("ID: %v", id)
	its.Equal(murphUser.Username, id.Username)
	its.Equal(murphUser.Email, id.Email)
	its.False(id.Admin)

	Unregister(its.opsCtx, murphUser.Username)
}

// TestRegisterUser - tests register
func (its *IdentityTestSuite) TestUnregisterUser() {

	err := Unregister(its.opsCtx, murphUser.Username)
	its.Require().Nil(err, ErrorStr(err))

	_, err = GetIdentity(its.opsCtx, murphUser.Username)
	// its.T().Logf("Get ID error: %v", err)
	its.NotNilf(err, "GetIdentity: %s", ErrorStr(err)) // we're expecting an id not found error

	s := ToGRPCStatus(err)
	its.Equal(codes.NotFound, s.Code())
}

func (its *IdentityTestSuite) TestRegisterBadPassword() {

	// Register olive with a bad password.
	od := *oliveUser
	od.Password = "1234"
	err := Register(&od)
	its.NotNil(err, ErrorStr(err)) // we're expecting an error

	// confirm it's a merge identity error.
	me := ToMergeError(err)
	its.ErrorIs(me, merror.ErrIdentityError)
	its.Contains(me.Detail, "The password must be") // from kratos may change on kratos updates
}

func (its *IdentityTestSuite) TestRegisterBadEmail() {

	// Register olive with a bad password.
	od := *oliveUser
	od.Email = "thisisnotanemailaddress"
	err := Register(&od)
	its.NotNil(err, ErrorStr(err)) // we're expecting an error

	// confirm it's a merge identity error.
	me := ToMergeError(err)
	its.ErrorIs(me, merror.ErrIdentityError)
	its.Contains(me.Detail, "is not valid \"email\"") // from kratos may change on kratos updates
}

func (its *IdentityTestSuite) TestRegisterBadUSState() {

	// If the user is in "United States", they must give a state.
	od := *oliveUser
	od.Country = "United States"
	od.Usstate = ""
	err := Register(&od)
	its.NotNil(err, ErrorStr(err)) // we're expecting an error

	// confirm it's a grpc InvalidArgument status
	s := ToGRPCStatus(err)
	its.Equal(codes.InvalidArgument, s.Code())
}
