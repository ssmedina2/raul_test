UTIL_SRC=login.go clients.go util_test.go data.go
ID_SRC=identity.go user.go
USER_SRC=user.go identity.go
ORG_SRC=org.go $(USER_SRC)
XDC_SRC=xdc.go $(USER_SRC)

OPSID=$(shell helm -n merge get values portal -o json | jq -r .opsId)
OPSPW=$(shell helm -n merge get values portal -o json | jq -r .opsPw)

VERBOSE=-test.v

TESTS=\
	  build/identity-test \
	  build/user-test \
	  build/org-test \
	  build/xdc-test

# This is expected to be overridden by CI/build.
CLUSTER=standalone

# minikube-portal
# CONTAINER=pops:latest
# CONTAINER_K8S=pops:latest
# PULL_POLICY=Never
# PUSH=0
# DOCKER=docker
# DOCKER_ARGS=
# DOCKER_PUSH_ARGS=

# merge appliance VTE
CONTAINER=localhost:5000/pops:latest
CONTAINER_K8S=host.internal:5000/pops:latest
PULL_POLICY=Always
PUSH=1
DOCKER=podman
DOCKER_ARGS=
DOCKER_PUSH_ARGS=--tls-verify=false

KC=kubectl -n merge

.PHONY: clean

all: run

tests: $(TESTS)

# likely a "makefile" way to abstract this.
build/identity-test: identity_test.go $(ID_SRC) $(UTIL_SRC)
	mkdir -p build
	go test -c $< $(ID_SRC) $(UTIL_SRC) -o $@

build/user-test: user_test.go $(USER_SRC) $(UTIL_SRC)
	mkdir -p build
	go test -c $< $(USER_SRC) $(UTIL_SRC) -o $@

build/org-test: org_test.go $(ORG_SRC) $(UTIL_SRC)
	mkdir -p build
	go test -c $< $(ORG_SRC) $(UTIL_SRC) -o $@

build/xdc-test: xdc_test.go $(XDC_SRC) $(UTIL_SRC)
	mkdir -p build
	go test -c $< $(XDC_SRC) $(UTIL_SRC) -o $@

copy_identity_test: build/identity-test
	$(KC) cp build/identity-test pops:/usr/local/bin

copy_user_test: build/user-test
	$(KC) cp build/user-test pops:/usr/local/bin

copy_org_test: build/org-test
	$(KC) cp build/org-test pops:/usr/local/bin

copy_xdc_test: build/xdc-test
	$(KC) cp build/xdc-test pops:/usr/local/bin


# likely a "makefile" way to abstract this.
run_user_test: build/user-test
	$(KC) exec -it pops -- /usr/local/bin/user-test $(VERBOSE)

run_identity_test: copy_identity_test
	$(KC) exec -it pops -- /usr/local/bin/identity-test $(VERBOSE)

run_org_test: copy_org_test
	$(KC) -n merge exec -it pops -- /usr/local/bin/org-test $(VERBOSE) 

run_xdc_test: copy_xdc_test
	$(KC) -n merge exec -it pops -- /usr/local/bin/xdc-test $(VERBOSE) 

pops_ctr: run-tests.sh
	$(DOCKER) build -f Dockerfile.pops -t $(CONTAINER) .
	$(ifeq ($(strip $(PUSH),1))
	$(DOCKER) push $(DOCKER_PUSH_ARGS) $(CONTAINER) 
	$(endif)
	$(KC) run pops \
		--image=$(CONTAINER_K8S) \
		--image-pull-policy=$(PULL_POLICY) \
		--env=OPSID=$(OPSID) \
		--env=OPSPW=$(OPSPW)
	$(KC) wait --for=condition=ready pods -l run=pops --timeout=20s

run_tests: run-tests.sh
	$(KC) cp ./run-tests.sh pops:/usr/local/bin/run-tests.sh

pops_tests: $(TESTS) run_tests
	$(KC) cp ./build pops:/tmp
	$(KC) exec -t pod/pops -- sh -c 'mv /tmp/build/* /usr/local/bin'

run: pops_tests
	$(KC) exec -it pops -- /usr/local/bin/run-tests.sh

repo_to_pops:
	$(KC) cp . pops:/root/repo

clean:
	$(RM) -rf build
	$(KC) exec -n merge pops -- rm -rf /usr/local/bin/*-test

pops_clean:
	$(KC) -n merge delete pod/pops
	$(DOCKER) image rm $(CONTAINER)
