package main

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"google.golang.org/grpc/metadata"
)

// Login the user with the given name and password. Return a context that
// can be passed into Merge API calls. The calls will run as if called
// by the user.
func LoginContext(uid, pw string, a *assert.Assertions) (context.Context, error) {

	var token string
	// TODO check and return error here.
	err := Identity(func(cli portal.IdentityClient) error {
		resp, err := cli.Login(
			context.TODO(),
			&portal.LoginRequest{
				Username: uid,
				Password: pw,
			},
		)

		a.Nilf(err, "WorkspaceClient.Login: %v", err)
		log.Infof("login resp: %v", resp)

		if resp != nil {
			token = resp.Token
		}

		return err
	})

	if err != nil {
		return nil, err
	}

	// add the token to the context.
	ctx := context.TODO()
	ctx = metadata.AppendToOutgoingContext(ctx, "Authorization", fmt.Sprintf("Bearer %s", token))

	return ctx, nil
}
