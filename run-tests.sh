#!/usr/bin/env bash

set -e

# get dir the script is in.
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

$SCRIPT_DIR/identity-test -test.v
$SCRIPT_DIR/user-test -test.v
$SCRIPT_DIR/org-test -test.v
$SCRIPT_DIR/xdc-test -test.v
