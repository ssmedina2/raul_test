package main

import (
	"fmt"
	"os"
	"testing"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/pkg/merror"
	"google.golang.org/grpc/status"
)

var (
	opsId string
	opsPw string
)

func TestMain(m *testing.M) {
	InitFromEnv()
	os.Exit(m.Run())
}

func InitFromEnv() {

	value, ok := os.LookupEnv("LOGLEVEL")
	if ok {
		lvl, err := log.ParseLevel(value)
		if err != nil {
			log.SetLevel(log.InfoLevel)
			log.Errorf("bad LOGLEVEL env var: %s. ignoring", value)
		} else {
			log.Infof("setting log level to %s", value)
			log.SetLevel(lvl)
		}
	}

	vals := map[string]*string{
		"OPSID": &opsId,
		"OPSPW": &opsPw,
	}

	for env, val := range vals {
		v, ok := os.LookupEnv(env)
		if !ok {
			log.Fatalf("Missing required environment variable %s", env)
		}
		log.Infof("Read %s value %s", env, v)
		*val = v
	}
}

// Convert an error into an error message string
func ErrorStr(err error) string {
	if err == nil {
		return ""
	}

	me := merror.FromGRPCError(err)

	if me.Detail == "Uncategorized" {
		if e, ok := status.FromError(err); ok {
			return fmt.Sprintf("GRPC Error: \"%s\" (%d)", e.Message(), e.Code())
		}
	}

	return mergeErrStr(me)
}

func mergeErrStr(me *merror.MergeError) string {

	s := ""
	if me.Title != "" {
		s += fmt.Sprintf("Title: %s\n", me.Title)
	}
	if me.Detail != "" {
		s += fmt.Sprintf("Detail: %s\n", me.Detail)
	}
	if me.Evidence != "" {
		s += fmt.Sprintf("Evidence: %s\n", me.Evidence)
	}
	if me.Instance != "" {
		s += fmt.Sprintf("Instance: %s\n", me.Instance)
	}
	if me.Type != "" {
		s += fmt.Sprintf("Type: %s\n", me.Type)
	}
	if me.Timestamp != "" {
		s += fmt.Sprintf("Timestamp: %s\n", me.Timestamp)
	}

	return s
}

func ToGRPCStatus(err error) *status.Status {
	if e, ok := status.FromError(err); ok {
		return e
	}

	return nil
}

func ToMergeError(err error) *merror.MergeError {
	return merror.FromGRPCError(err)
}
