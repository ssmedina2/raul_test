package main

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"google.golang.org/grpc/codes"
)

type OrgTestSuite struct {
	suite.Suite // struct composition to include all suite deatils.

	opsCtx  context.Context // login context of ops
	userCtx context.Context // login context of user

	userData   *UserRegisterData // org creator user data
	memberData *UserRegisterData // member user data
	orgData    *OrgData
}

// Invoke thte test suite via a normal go test
func TestOrganization(t *testing.T) {
	suite.Run(t, &OrgTestSuite{})
}

func (ots *OrgTestSuite) SetupSuite() {

	ots.userData = oliveUser
	ots.memberData = murphUser
	ots.orgData = oliveOrg

	var err error
	// get OPS auth context
	ots.opsCtx, err = LoginContext(opsId, opsPw, ots.Assertions)
	ots.Require().Nil(err, ErrorStr(err))

	err = AddActiveUser(ots.userData, ots.opsCtx, ots.Assertions)
	ots.Require().Nil(err, ErrorStr(err))

	// get org creator auth context.
	ots.userCtx, err = LoginContext(ots.userData.Username, ots.userData.Password, ots.Assertions)
	ots.Require().Nil(err, ErrorStr(err))
}

func (ots *OrgTestSuite) TearDownSuite() {

	// kill the creator/user
	err := DeleteActiveUser(ots.userData.Username, ots.opsCtx, ots.Assertions)
	ots.Nil(err, ErrorStr(err))
}

func (ots *OrgTestSuite) BeforeTest(suiteName, testName string) {

	switch testName {
	case "TestOrgCreate":
		err := CreateOrg(ots.userCtx, ots.orgData)
		ots.Require().Nil(err, ErrorStr(err))
	case "TestActivateOrg":
		err := CreateOrg(ots.userCtx, ots.orgData)
		ots.Require().Nil(err, ErrorStr(err))
	case "TestNoDuplicateOrgs":
		fallthrough
	case "TestCreateMulitpleOrgs":
		fallthrough
	case "TestOrgActivateNonUserMember":
		fallthrough
	case "TestOrgUpdate":
		ots.createActiveOrg(ots.userCtx, ots.orgData)
	}
}

func (ots *OrgTestSuite) AfterTest(suiteName, testName string) {

	switch testName {
	case "TestOrgCreate":
		// BUG issue: https://gitlab.com/mergetb/portal/services/-/issues/358
		// We need to use OPS context to delete even though we should be able to
		// use the org maintainer context.
		// err := DeleteOrg(ots.userCtx, ots.orgData)
		err := DeleteOrg(ots.opsCtx, ots.orgData)
		ots.Require().Nilf(err, ErrorStr(err))
	case "TestOrgActivateNonUserMember":
		// Delete the member account we created.
		err := DeleteActiveUser(ots.memberData.Username, ots.opsCtx, ots.Assertions)
		ots.Require().Nil(err, ErrorStr(err))
		fallthrough
	default:
		err := DeleteOrg(ots.userCtx, ots.orgData)
		ots.Require().Nilf(err, ErrorStr(err))
	}
}

// helper function.
func (ots *OrgTestSuite) createActiveOrg(userCtx context.Context, od *OrgData) {

	ots.T().Logf("creating active org %+v", od)
	err := CreateOrg(userCtx, od)
	ots.Require().Nil(err, ErrorStr(err))
	err = ActivateOrg(ots.opsCtx, od)
	ots.Nil(err, ErrorStr(err))
}

func (ots *OrgTestSuite) TestOrgCreate() {

	org, err := GetOrg(ots.opsCtx, ots.orgData)
	ots.Require().Nil(err, ErrorStr(err))

	// Confirm the data is correct.
	ots.Equal(ots.orgData.Name, org.Name)
	ots.Equal(ots.orgData.Description, org.Description)
	ots.Equal(portal.UserState_Pending, org.State) // not approved in SetupSuite()
	ots.Equal(portal.AccessMode_Public, org.AccessMode)
	ots.Equal(ots.orgData.Category, org.Category)
	ots.Equal(ots.orgData.Subcategory, org.Subcategory)

	ots.T().Logf("get org: %+v", org)
	ots.T().Logf("expected org: %+v", ots.orgData)
}

func (ots *OrgTestSuite) TestActivateOrg() {
	// Activate org as OPS
	err := ActivateOrg(ots.opsCtx, ots.orgData)
	ots.Nil(err, ErrorStr(err))

	// Get the org and confirm it is active
	org, err := GetOrg(ots.opsCtx, ots.orgData)
	ots.Nil(err, ErrorStr(err))

	ots.Equal(portal.UserState_Active, org.State)
}

func (ots *OrgTestSuite) TestOrgUpdate() {

	newDesc := "this is the new desc"
	err := UpdateOrgDescription(ots.opsCtx, ots.orgData.Name, newDesc)
	ots.Nil(err, ErrorStr(err))

	org, err := GetOrg(ots.opsCtx, ots.orgData)
	ots.Nil(err, ErrorStr(err))

	ots.Equal(newDesc, org.Description)
}

func (ots *OrgTestSuite) TestNoDuplicateOrgs() {

	// Create the org again. This should fail with "already exists".
	err := CreateOrg(ots.userCtx, ots.orgData)
	ots.Require().NotNil(err, ErrorStr(err))

	// Convert to GRPC error and confirm the "already exists" error
	s := ToGRPCStatus(err)
	ots.Equal(codes.AlreadyExists, s.Code())
}

func (ots *OrgTestSuite) TestCreateMulitpleOrgs() {

	count := 1
	orgDatas := []OrgData{}

	// clean up created orgs by hand after test is over.
	defer func() {
		// There is no DeleteOrganizations API so we delete each by hand
		for _, od := range orgDatas {
			err := DeleteOrg(ots.userCtx, &od)
			ots.Nil(err, ErrorStr(err))
		}
	}()

	// create `count` orgs.
	for i := 0; i < count; i++ {
		od := *ots.orgData                        // copy value
		od.Name = fmt.Sprintf("orginstance%d", i) // change the org name then create.
		ots.T().Logf("org data: %+v", od)
		ots.createActiveOrg(ots.userCtx, &od)
		orgDatas = append(orgDatas, od)
	}

	// Confirm they are there.
	// FIX NEEDED: StatusMS needed for GetOrganizations() call.
	orgs, err := GetOrgs(ots.userCtx) // get orgs for this user
	ots.Require().Nil(err, ErrorStr(err))

	ots.Equal(count+1, len(orgs)) // +1 as we create the default org in the test setup

	// TODO - confirm the orgs are there and named correctly
}

// TestOrgActivateNonUserMember - org maintainers, given a standard policy,
// can activate users that have applied for membership in thier orgs. When they
// approve the membership, the portal should init and activate the accounts.
func (ots *OrgTestSuite) TestOrgActivateNonUserMember() {

	// org is active here. register a new user which requests membership in the org.
	// "murph" user is not used in these tests so far, so use that one
	member := murphUser
	err := Register(member)
	ots.Require().Nil(err, ErrorStr(err))

	memberCtx, err := LoginContext(member.Username, member.Password, ots.Assertions)
	ots.Require().Nil(err, ErrorStr(err))

	// Request org membership as a user and a org member.
	err = RequestOrgMembership(
		memberCtx,
		member.Username,
		ots.orgData.Name,
		portal.MembershipType_UserMember,
		portal.Member_Member,
	)
	ots.Require().Nil(err, ErrorStr(err))

	// confirm the user is not active (this is not strictly needed as we check for this in
	// other tests).
	u, err := GetUser(ots.opsCtx, member.Username)
	ots.Require().Nil(err, ErrorStr(err))
	ots.Equal(portal.UserState_Pending, u.State)

	// confirm the user membership request is pending according to the user
	ots.Require().Contains(u.Organizations, ots.orgData.Name)
	ots.Equal(u.Organizations[ots.orgData.Name].Role, portal.Member_Member)
	ots.Equal(u.Organizations[ots.orgData.Name].State, portal.Member_MemberRequested)

	// confirm the user membership request is pending according to the org
	org, err := GetOrg(ots.userCtx, ots.orgData)
	ots.Nil(err, ErrorStr(err))
	ots.Require().Contains(org.Members, member.Username)
	ots.Equal(org.Members[member.Username].Role, portal.Member_Member)
	ots.Equal(org.Members[member.Username].State, portal.Member_MemberRequested)

	// approve the membership as the org creator (olive)
	err = ConfirmOrgMembership(
		ots.userCtx,
		member.Username,
		ots.orgData.Name,
		portal.MembershipType_UserMember,
	)
	ots.Nil(err, ErrorStr(err))

	// now confirm the user is active and has a gid/uid and is in the org
	u, err = GetUser(ots.opsCtx, member.Username)
	ots.Require().Nil(err, ErrorStr(err))

	ots.Equal(portal.UserState_Active, u.State)
	ots.Positive(u.Gid)
	ots.Positive(u.Uid)
	ots.Require().Contains(u.Organizations, ots.orgData.Name)
	ots.Equal(u.Organizations[ots.orgData.Name].Role, portal.Member_Member)
	ots.Equal(u.Organizations[ots.orgData.Name].State, portal.Member_Active)
	ots.Len(u.Organizations, 1)
	ots.Len(u.Projects, 1)

	// Confirm the org thinks the member is a member.
	mems, err := GetOrgMembers(ots.userCtx, ots.orgData)
	ots.Nil(err, ErrorStr(err))

	ots.T().Logf("org members: %+v", mems)
	ots.Contains(mems, member.Username)
}
