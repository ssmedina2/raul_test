package main

import (
	"context"
	"fmt"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func CreateOrg(ctx context.Context, od *OrgData) error {

	err := Workspace(
		func(cli portal.WorkspaceClient) error {
			_, err := cli.CreateOrganization(
				ctx,
				&portal.CreateOrganizationRequest{
					User: od.Creator,
					Organization: &portal.Organization{
						Name:        od.Name,
						Description: od.Description,
						AccessMode:  od.Mode,
						Category:    od.Category,
						Subcategory: od.Subcategory,
					},
				},
			)
			return err
		},
	)

	return err
}

func DeleteOrg(ctx context.Context, od *OrgData) error {

	err := Workspace(
		func(cli portal.WorkspaceClient) error {
			_, err := cli.DeleteOrganization(
				ctx,
				&portal.DeleteOrganizationRequest{
					User: od.Creator,
					Name: od.Name,
				},
			)
			return err
		},
	)

	return err
}

func GetOrg(ctx context.Context, od *OrgData) (*portal.Organization, error) {

	var org *portal.Organization

	err := Workspace(func(cli portal.WorkspaceClient) error {
		resp, err := cli.GetOrganization(
			ctx,
			&portal.GetOrganizationRequest{
				Name:     od.Name,
				StatusMS: 10000, // wait seconds for response.
			},
		)
		if resp != nil {
			org = resp.Organization
		}
		return err
	})

	return org, err
}

func GetOrgs(ctx context.Context) ([]*portal.Organization, error) {

	var orgs []*portal.Organization

	err := Workspace(func(cli portal.WorkspaceClient) error {
		resp, err := cli.GetOrganizations(
			ctx,
			&portal.GetOrganizationsRequest{
				Filter: portal.FilterMode_ByAll,
			},
		)
		if resp != nil {
			orgs = resp.Organizations
		}
		return err
	})

	return orgs, err
}

func ActivateOrg(ctx context.Context, od *OrgData) error {

	err := Workspace(func(cli portal.WorkspaceClient) error {
		_, err := cli.ActivateOrganization(
			ctx,
			&portal.ActivateOrganizationRequest{
				Organization: od.Name,
			},
		)
		return err
	})

	return err
}

func UpdateOrgDescription(ctx context.Context, name, desc string) error {

	err := Workspace(func(cli portal.WorkspaceClient) error {
		_, err := cli.UpdateOrganization(
			ctx,
			&portal.UpdateOrganizationRequest{
				Name: name,
				Description: &portal.DescriptionUpdate{
					Value: desc,
				},
			},
		)
		return err
	})

	return err
}

// note no context as auth is not required to request membership
func RequestOrgMembership(ctx context.Context, username, orgname string, kind portal.MembershipType, role portal.Member_Role) error {

	err := Workspace(func(cli portal.WorkspaceClient) error {
		_, err := cli.RequestOrganizationMembership(
			ctx,
			&portal.RequestOrganizationMembershipRequest{
				Organization: orgname,
				Id:           username,
				Kind:         kind,
				Member: &portal.Member{
					Role: role,
				},
			},
		)
		return err
	})

	return err
}

func ConfirmOrgMembership(ctx context.Context, membername, orgname string, kind portal.MembershipType) error {

	err := Workspace(func(cli portal.WorkspaceClient) error {
		_, err := cli.ConfirmOrganizationMembership(
			ctx,
			&portal.ConfirmOrganizationMembershipRequest{
				Organization: orgname,
				Id:           membername,
				Kind:         kind,
			},
		)
		return err
	})

	return err
}

func GetOrgMembers(ctx context.Context, od *OrgData) (map[string]*portal.Member, error) {

	// see https://gitlab.com/mergetb/portal/services/-/issues/359 fore why we do not call
	// cli.GetOrganizationMembers().
	o, err := GetOrg(ctx, od)
	if err != nil {
		return nil, err
	}

	if o != nil && o.Members != nil {
		return o.Members, nil
	}

	return nil, fmt.Errorf("no members in organization")
}
