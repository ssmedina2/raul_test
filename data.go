package main

import (
	portal "gitlab.com/mergetb/api/portal/v1/go"
)

// data typoes
type UserRegisterData struct {
	Username    string
	Email       string
	Password    string
	Institution string
	Category    string
	Name        string
	Country     string
	Usstate     string
}

func NewUserRegisterData(uid, email, pw, inst, cat, name, country, state string) *UserRegisterData {
	return &UserRegisterData{
		Username:    uid,
		Email:       email,
		Password:    pw,
		Institution: inst,
		Category:    cat,
		Name:        name,
		Country:     country,
		Usstate:     state,
	}
}

type OrgData struct {
	Name        string
	Creator     string
	Description string
	Category    string
	Subcategory string
	Mode        portal.AccessMode
}

func NewOrgData(name, creator, desc, cat, subcat string, mode portal.AccessMode) *OrgData {
	return &OrgData{
		Name:        name,
		Creator:     creator,
		Description: desc,
		Category:    cat,
		Subcategory: subcat,
		Mode:        mode,
	}
}

// data instantiations.
var (
	//
	// Users
	//
	murphUser = NewUserRegisterData(
		"murphy",
		"murphy@example.net",
		"muffins5!",
		"The Murphtones",
		"Dev Pets",
		"The Murphster",
		"United States",
		"California",
	)

	oliveUser = NewUserRegisterData(
		"olive",
		"olive@example.net",
		"muffins5!",
		"The Olive Institute",
		"Dev Pets",
		"Olive Cat",
		"Canada",
		"",
	)

	// User map
	userMap = map[string]*UserRegisterData{
		murphUser.Username: murphUser, // "murphy"  ==> murphy user data.
		oliveUser.Username: oliveUser,
	}

	//
	// Orgs
	//
	oliveOrg = NewOrgData(
		"oliveorg",
		userMap["olive"].Username,
		"Olive's Organization",
		"Research",
		"String Theory",
		portal.AccessMode_Public,
	)

	// Org map
	orgMap = map[string]*OrgData{
		oliveOrg.Name: oliveOrg, // "oliveOrg" ==> Olive Org data
	}
)
