package main

import (
	"fmt"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var (
	apiService = "apiserver"
	apiPort    = "6000"

	// GRPC config.
	GRPCMaxMessageSize = 512 * 1024 * 1024
	GRPCMaxMessage     = grpc.WithDefaultCallOptions(
		grpc.MaxCallRecvMsgSize(GRPCMaxMessageSize),
		grpc.MaxCallSendMsgSize(GRPCMaxMessageSize),
	)
)

func connection() (*grpc.ClientConn, error) {

	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%s", apiService, apiPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		GRPCMaxMessage,
	)
	if err != nil {
		return nil, fmt.Errorf("bad connection to %s", apiService)
	}

	return conn, nil
}

func Identity(f func(portal.IdentityClient) error) error {

	conn, err := connection()
	if err != nil {
		return err
	}
	defer conn.Close()
	return f(portal.NewIdentityClient(conn))
}

func Workspace(f func(portal.WorkspaceClient) error) error {

	conn, err := connection()
	if err != nil {
		return err
	}
	defer conn.Close()
	return f(portal.NewWorkspaceClient(conn))
}

func XDC(f func(portal.XDCClient) error) error {

	conn, err := connection()
	if err != nil {
		return err
	}
	defer conn.Close()
	return f(portal.NewXDCClient(conn))
}
